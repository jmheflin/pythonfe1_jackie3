#pfe1 might we be friends project jaquelyn h

#intro statement
print("Hi! Welcome to our pet rescue. Let's find you a friend!")
print("what is your name?")
input_name= input()
print("Hi, %s, take our short test to see what pet will suit you. Based on your score, we will be able to find a pet that fits you best!" % input_name)

#create variable for user's score
user_score= 0

#question 1
#every yes answer is +10. every no answer is -10
print("do you like pets? (1=yes 0=no)")
q1_input= int(input())

if q1_input == 1:
    user_score = user_score + 10
elif q1_input == 0:
    user_score = user_score - 10
print("noted. next question!")

#question 2
print("do you have a pet already? (1=yes 0=no)")
q2_input=int(input())

if q2_input == 1:
    user_score = user_score + 10
elif q2_input == 0:
    user_score = user_score - 10
print("noted. next question!")

#question 3
print("do you like outdoor activities with your pet? (1=yes 0=no)")
q3_input= int(input())

if q3_input == 1:
    user_score = user_score + 10
elif q3_input == 0:
    user_score = user_score - 10
print("noted. next question!")

#question 4
print("do you like to take your pet wherever you go? (1=yes 0=no)")
q4_input= int(input())

if q4_input == 1:
    user_score = user_score + 10
elif q4_input == 0:
    user_score = user_score - 10
print("noted. next question!")

#question 5
print("do you spend a lot of time at home? (1=yes 0=no)")
q5_input= int(input())

if q5_input == 1:
    user_score = user_score + 10
elif q5_input == 0:
    user_score = user_score - 10
print("noted. next question!")

#question 6
print("do you have a yard? (1=yes 0=no")
q6_input= int(input())

if q6_input == 1:
    user_score = user_score + 10
elif q6_input == 0:
    user_score = user_score - 10
print("that concludes the search. let's see your results!")
print("your score is %s" % user_score)

if user_score < 0:
    print("hmm...it looks like you need a low maintenance pet. let's find one!")
elif 0 < user_score < 30:
    print("hmm...depending on yor pet's personality, you can have any pet you'd like!")
elif 30 < user_score:
    print("hmm...the options are endless!")

print("thanks for taking our compatibility test. good luck with your new friend!")
